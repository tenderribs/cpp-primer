#include <iostream>
#include <array>
#include <string>
using std::cout;
using std::cin;
using std::endl;
using std::array;

int main() {
    int i;
    char character;
    signed signedInt;
    unsigned long long int unsignedLongLongInt;
    std::string str;

    cout << "i:" << sizeof(i) << endl;
    cout << "character:" << sizeof(character) << endl;
    cout << "signedInt:" << sizeof(signedInt) << endl;
    cout << "unsignedLongLongInt:" << sizeof(unsignedLongLongInt) << endl;
    cout << "str:" << sizeof(str) << endl;

    return 0;
}