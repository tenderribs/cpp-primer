# 4 Expressions
### 4.1 Fundamentals
Unary operators such as the address of & operator or the dereference operator * act on one operand. Binary operators such as multiplication act on two.

rvalue: Expression that yields a value but not the associated location, if any, of that value.
lvalue An expression that yields an object or function. A nonconst lvalue that denotes an object may be the left-hand operand of assignment.

We can use an lvalue when an rvalue is required, but we cannot use an rvalue (value) when a lvalue (location) is needed.

When applying decltype to an expression, the result is a reference type if the expression yields an lvalue

Precedence and associativity dictate the grouping of compound expressions. Div and multipl. have the same but higher precedence than add. and subtr. Left to right. Override grouping with parenthesis.

Consider the following
```cpp
int ia[] = {0,2,4,6,8}; // array with five elements of type int
int last = *(ia + 4); // initializes last to 8, the value of ia [4]
last = *ia + 4; // last = 4, equivalent to ia [0] + 4
```

For operators that do not specify eval order, it is wrong to refer to and change the same object "at once".

#### Two rules of thumb:
1. When in doubt, use parenthesis to group expressions
2. If the value of an operand is changed, it shouldnt be used anywhere else in the same expression

### 4.2 Arithmetic operators

Division between integers truncates decimal points. The modulo operator computes the remainder that results from division. Both operators have to be of integer type.
Overflow means the value computed from the variables is out of the range of values that the type of the variable can hold.

### 4.3 Logical and relational operators

The logical AND and OR operators evaluate their left operator before the right. Therefore, the right side of an && is evaluated only if the left side is true. The right side of a || operator is only evaluated if the left side is false. --> short circuit evaluation

```cpp
if (val) { /* ... */ } // true if val is any nonzero value
if (!val) { /* ... */ } // true if val is zero
```
It is usually a bad idea to use the boolean literals true and false as operands in a comparison. These literals should be used only to compare to an object of type bool.

### 4.4 Assignment Operator
Assignment is right hand associative. Since assignment returns its left hand operand, the result of the right most assignment is assigned to ival.

```cpp
int ival, jval;
ival = jval = 0; // ok: each assigned 0, type matches for ival and jval
```

Assignment has lower precedence than the relational operators used in conditional statements. Parenthesis are usually needed around assignments in conditions.

### 4.5. Increment and Decrement Operators

There are two forms of these operators: Prefix and postfix. The prefix form increments its operand and yields the changed object as its  result. The  postfix operators increment the operand but yield a copy of the original.

```cpp
    int i = 0, j;
    j = ++i; // j = 1, i = 1: prefix yields the incremented value
    j = i++; // j = 1, i = 2: postfix yields the unincremented value
```

The postfix versions are used when the current value of a variable is to be used and incremented in a single expression.

```cpp
auto pbeg = v.begin();
// print elements up to the first negative value
while (pbeg != v.end() && *beg >= 0)
    cout << *pbeg++ << endl; // print the current value and advance pbeg
```
Here *pbeg++ is really just *(pbeg++) due to precedence rules. This usage relies on the fact that the postfix increment returns a copy of the original, unincremented operand. Yet it still advances the iterator during the same iteration. If it were a prefix, then we would skip the first element of v.

#### 4.6 The conditional operator ?

The conditional operator is a nice way to implement logic in an expression.

The syntax is `(cond) ? expression1 : expression2`;

```cpp
string finalgrade = (grade < 60) ? "fail" : "pass";
```

#### 4.9 Sizeof Operator

The sizeof operator returns the size of an expression or type name in bytes. The result is a constant expression of type `size_t`.

#### 4.11 Type conversions

Two types are related if there is a conversion between them. A related type can then be used in an expression where a compatible type is expected. The types types are implicitly transformed into a common type.

The initializer expression is converted to the type of the object. In assignments the right hand operand is converted to the left hand's type. The conversions happen according to the following sequence.

- Small integral types (char, short, bool) are converted to int (Integral promotion)
- signed int will be converted to unsigned int --> <b>wrap around will mess up results</b>

When we use an array, the array is automatically converted to a pointer to the first element of the array.

Class types also define their own conversions. For instance the IO library defines a conversion from istream to bool which is used to convert cin to bool:

```cpp
while (cin >> s) {
    /* do something pls */
}
```

##### Casts

Casts request explicit conversions and are quite dangerous. There is no error or warning from the compiler because casts tell the compiler that a certain conversion is okay. The conversion can possibly be quite meaningless (address of int to char etc) and hard to find.

```cpp
int i, j;
double slope = i/j; //explicitly convert int to double
```
