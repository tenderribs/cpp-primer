# 1 Getting Started

## Composite types

### references

references create an indirect link to an object. they are basically aliases that are bound to an object and will mutate the object associated with it. Note that they arent objects though (and therefore dont have to be initialized).

``` cpp
int main() {
    int i = 42;

    int &ref = i // ref is a reference to i
}
```

### pointers

pointers are similar to references, since the create an indirect link to an object. But the difference is that they can be reassigned to other objects

``` cpp
int main() {
    int i = 42;

    int *p = &i // p is a pointer to the object at address of (& operator) i
}
```

using the * symbol in front of a pointer (as left hand operand), the pointer is dereferenced and the object that the pointer references can be modified.

It is tricky when to know when the pointer is changed or the object to which the pointer points.

``` cpp
int main() {
    int val = 420;

    *p = nullptr;

    p = &i; // p is changed to point to a different object

    *p = 0; // the object at address i is changed ie the value to which p points.
}
```