#include "Sales_item.h"
#include <iostream>

int main() {
    Sales_item transaction, currTransaction;

    std::cout << "enter a list of transactions" << std::endl;

    if (std::cin >> transaction) {
        currTransaction = transaction;
        int ctr = 1;

        while (std::cin >> transaction) {
            std::cout << transaction << std::endl;

            if (transaction.isbn() == currTransaction.isbn()) {
                ctr += 1;
            } else {
                std::cout << "The transaction " << transaction << " occured " << ctr << " times." << std::endl;
                currTransaction = transaction;
                ctr = 1;
            }
        }
    } else {
        return -1;
    }
    return 0;
}