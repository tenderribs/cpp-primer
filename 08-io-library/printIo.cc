#include <iostream>

using namespace std;

istream& func(istream &is) {
	string input;

	while (is >> input)
		cout << input << endl;

	return is;
}

int main() {
	istream& is = func(std::cin);
	std::cout << is.rdstate() << std::endl;
	std::cout << is.eof() << std::endl;

	return 0;
}

