# 6 Functions

A function is a block of code with a name that can be called. Functions can be overloaded, meaning the same name may refer to several different functions.

## 6.1 Function Basics

When calling a function, the parameters are implicity defined and initialized. Execution of the calling function is then suspended and execution of the called function begins.

The type of the supplied params must match the type of the arguments. Arguments are comma separated. Functions have a return type that arent allowed to be of array or function type. Objects have a lifetime next to a scope which describes when they exist. Objects defined outside of a function exist throughout the programs execution. The lifetime of a local variable depends on how it is defined.

- Objects that exist only while a block is executing are known as `automatic objects`. After execution an automatic object's value is undefined. Parameters are examples for automatic objects.
- Local static objects are not destroyed when a functions ends, they are destroyed when the program terminates.

```cpp
size_t count_calls() {
    static size_t ctr = 0; // value will persist across calls
    return ++ctr;
}

int main() {
    for (size_t i = 0; i != 10; ++i)
    cout << count_calls() << endl;
    return 0;
}
```

A function must be declared before it is used. It can be defined only once, but declared many times. Functions should be declared in header files and defined in source files. Like this we ensure that all the declarations for a given function agree. The source file that defines the function should include the header with the declaration. Then the compiler will verify consistency across header and source.

### Separate compilation

To allow the program to be written in logical parts, the language provides something called separate compilation. Programs can be logically separated and split into different files.

Declare the existence of the function in a header function. Run g++ -c to export a object code file.


## 6.2 Argument Passing

The type of the parameter defines what happens with the argument. Reference types are said to be passed by reference and the parameter is an alias for its corresponding argument.
Normal types are "passed by value".

Static variable are not destroyed when the function ends. --> maybe the connection count variable should be static in client side