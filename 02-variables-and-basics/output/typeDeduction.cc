int main() {
    const int i = 42;
    auto j = i; const auto &k = i; auto *p = &i;
    const auto j2 = i, &k2 = i;

    // j is a int
    // k is a const int&
    // p is const int*
    // j2 is const int

    // k2 is int&

    // REMEMBER THAT TYPE IN COMMA SEPARATED DECLARATION IS INHERITED ALSO BY CHILDREN
}