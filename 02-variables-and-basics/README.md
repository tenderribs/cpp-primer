# 2 Variables and basics

## 2.5 Dealing with Types
### Top and bottom level const

A pointer is an object that can point to another object. Either of them can be a const. If the pointer is const, then it called a top level const. If the object that is pointed to is const, then it is a bottom level const.

``` cpp

int main() {
    int i = 0;
    int *const p1 = &i; // p1 cannot be changed. It is a top level const

    const int = 42;

    const int *p2 =  &ci; // p2 can be changed. It is a low level const
}
```

When we copy an object, the top level const is ignored (Copying doenst change the copied object anyway). Low level const are not ignored when copying.

### constant expressions

A constexpr is an expression whose value cannot change i.e. when it is initialized with a literal --> can be read at compile time already. Use the type def constexpr for vars that are intended for use as const expressions.


### auto and decltype

These are to be used when you dont know what the type of an expression is. They are inferred by the type of the literal (compulsory as right hand operand). The differences are:

- Auto drops top level consts, decltype keeps them.
- the deduced type of decltype depends on its given expression. If enclosed in double parentheses, it will always yield a reference compound type. Single parenthesis will only yied reference if var is actually a reference

``` cpp
int main () {
    const int i = 42;
    auto j = i; const auto &k = i; auto *p = &i;
    const auto j2 = i, &k2 = i;

    // j is a int
    // k is a const int&
    // p is const int*
    // j2 is const int

    const int ci = 0, &cj = ci;
    decltype(ci) x = 0; // x has type const int
    decltype(cj) y = x; // y has type const int& and is bound to x
    decltype(cj) z; // error: z is a reference and must be initialized
}
```

## 2.6 Defining our own Data Structures
### Create a class

``` cpp

struct foo {
    unsigned int bar = 0; // first data member of class with in class initializer
    double baz = 0.0;
};

// remember to use a semicolon after class body
```

It makes sense to decouple the classes from the program you are writing for reusage in other files. For this there are header files. Since the header can include other headers, which might appear as duplicates in multiple files, we write them with header guards. They prevent multiple inclusion like follows:

```cpp
#ifndef SALES_DATA_H  //succeeds first time sales_data.h is included, process lines up until #endif
#define SALES_DATA_H
#include <string>

struct Sales_data {
    std::string bookNo;
    unsigned units_sold = 0;
    double revenue = 0.0;
};
#endif

/* Headers should have guards, even if they aren’t (yet) included by another header. Header guards are trivial to write, and by habitually defining them
 you don’t need to decide whether they are needed. */
```
