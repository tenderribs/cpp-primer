struct Foo { /* empty */ }; // Note: no semicolon
int main()
{
return 0;
}

//(error: expected ';' after struct definition
// struct Foo { /* empty */ } // Note: no semicolon)