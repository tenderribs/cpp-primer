#include <iostream>

int main() {
    // int a = 3, b = 4;
    // decltype(a) c = a;
    // decltype((b)) d = a;
    // ++c;
    // ++d;

    // //a is int, b is int

    // // c is int, val is 3
    // // d is int&, val is 3
    // std::cout <<  "c: " << c << std::endl;
    // std::cout <<  "d: " << d << std::endl;

    int a = 3, b = 4;
    decltype(a) c = a;
    decltype(a = b) d = a;
}