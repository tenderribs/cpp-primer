module.exports = {
    title: 'C++ Primer',
    description: 'Log of reading C++ primer, with notes about the new things I learned.',
    head: [
        ['link', { rel: 'icon', href: '/img/logo.png' }]
    ],
    themeConfig: {
        nav: [
            { text: 'Home', link: '/' },
            { text: 'C++ Primer PDF', link: 'https://gitlab.com/tenderribs/cpp-primer/-/blob/master/c++_primer.pdf'},
            { text: 'Answers', link: 'https://github.com/jaege/Cpp-Primer-5th-Exercises' },
        ],
        logo: '/img/logo.png',
        displayAllHeaders: true,
        activeHeaderLinks: true,
        smoothScroll: true,
        repo: 'https://gitlab.com/tenderribs/cpp-primer',
        sidebar: [
            {
                title: 'Installation',
                collapsable: true,
                children: [
                    '/00-installation/',
                ]
            },
            {
                title: 'Chapters',
                collapsable: false,
                children: [
                    '/01-getting-started/',
                    '/02-variables-and-basics/',
                    '/03-strings-vectors-and-arrays/',
                    '/04-expressions/',
                    '/05-statements/',
                    '/06-functions/',
                ]
            },
        ]
    }
}

// https://vuepress.vuejs.org/theme/default-theme-config.html