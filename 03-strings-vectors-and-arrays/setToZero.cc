#include <iostream>
#include <array>
using std::cout;
using std::cin;
using std::endl;
using std::array;

int main() {
    int numbers[] = {2, 1, 4, 2, 5};
    int *end = &numbers[5];

    for (int *el = numbers; el != end; ++el) {
        cout << *el << endl;

        *el = 0;
    }
    cout << "Changed all the numbers to zero" << endl;

    for (int *el = numbers; el != end; ++el) {
        cout << *el << endl;
    }
}