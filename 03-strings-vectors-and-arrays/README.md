# 3 Strings, Vectors and Arrays

## 3.1 Namespace using declarations

A using declaration lets us access a member from a namespace directly.

```cpp
#include <iostream>
// using declaration; when we use the name cin, we get the one from the namespace
std
using std::cin;
int main()
{
    int i;
    cin >> i; // ok: cin is a synonym for std::cin
    cout << i; // error: no using declaration; we must use the full name
    std::cout << i; // ok: explicitly use cout from namepsace std

    return 0;
}
```

## 3.2 String type
### operations
```cpp

string s;

cout << s           //write s onto output stream
cin >> s            //read whitespace separated string from input stream

getLine(cin, s)     //read line of input from cin into s, return s (excl the return character)
s.empty()           //return bool if string is empty
s.size()            //return size of string as string::size_type

/*
    otherwise there are all comparison operators such as == and !==
 */
```

### quirks

- string::size_type is unsigned, so be careful when comparing with signed ints that will wrap around when compared.
Example: neg signed int will be larger than size_type when wrapped around.

- It is not possible to concat string literals, despite it being possible to concat strings.

```cpp
string1 + string2 //works

"Foo" + "Bar" //error
```

### cctype

### Some important functions
```cpp

isalnum(c)          //letter or digit
isalpha(c)          //is a letter
iscntrl(c)          //control character
isdigit(c)          //digit
isgraph(c)          //not space but printable
islower(c)          //lowercase
isupper(c)          //uppercase
isprint(c)          //printable
isxdigit(c)         //is hexadecimal
```

### accessing chars in string

range for statement to access all chars:

for (declaration : statement)
    statement

using subscripts you can acces single characters:
s[0] but make sure that when iterating in for loop the subscript accessor does not exceed the strings length.

## 3.3 Vectors

A vector is not a type, but a template that can hold objects of certain type (like a container). Templates tell the compiler to instantiate specified functions and classes. The element type must be included to use a vector type. You can either list initialize a vector or construct it.

```cpp
    #include std::vector;
    using std::vector;

    vector<int> ivec; // default initialization; svec has no elements
    vector<string> foobar = {"foo", "bar", "baz"};
    vector<int> v1(10); // v1 has ten elements with value 0
    vector<int> v2{10}; // v2 has one element with value 10
    vector<int> v3(10, 1); // v3 has ten elements with value 1
    vector<int> v4{10, 1}; // v4 has two elements with values 10 and 1
```

Push elements to vector at runtime with push_back member. Default initializing a vector and pushing to it is better for performance than constructing or initializing a vector of a certain size. It is not possible to use a range for if the loop's body adds to the vector.

Access element of vector with subscript.
Subscript operator fetches an existing element, does not add additional one. Referencing an element that does not exist will lead to runtime errors. These errors are known as buffer overflows.

## 3.4 Container iterators

Container types have member functions, which fetch elements and have operations to move from one element to another. They provide indirect access to the element and similar to pointers can be dereferenced. The off the end iterator cannot be dereferenced or incremented since it doesnt denote an element. Use the ++ operator to advance the position of the iterator by one.

### Iterator type

The type return by begin and end is either const_iterator (only read only access for element) or iterator (read and write access for element)
use cbegin or cend members if only read access is necessary

### Iterator arithmetic
```cpp
iter + n
iter - n
iter1 += n
iter1 -= n
iter1 - iter2
// NOTE: there is no + arithmetic operator on two iterators. Only difference.

>, >=, <, <=
```

The difference between two different iterators returns a signed type called the difference_type.


## 3.5 Arrays

Arrays are similar to vectors in the sense that they are containers of compound type that can be accessed by subscript operators or iterators. The difference is that their size is fixed, therefore no elements can be added or removed.

``` cpp
    int main() {
        #include <string>
        using std::string;

        constexpr unsigned int size = 10; // size has to be of type constexpr
        string arr[size]; // type has to be specified, cannot deduce type from list of initializers

        unsigned scores[11] = {}; 11 //buckets, all default initialized
    }

```

Note that when list initializing a string from chars with a specified dimension, space has to be left for the null character ('\0') that is added at the end of every string.
The following example wont work:
``` cpp
const char a4[6] = "Daniel"; // error: no space for the null!
```
### 3.5.2. Accessing the Elements of an Array

It is not possible to create an array of references since they arent objects.

It is best to traverse all elements with a ranged for, but their

``` cpp
for (auto element : myArray)  // for each element in myArray
```
An array actually just performs operations on pointers to objects. Thus, the auto type will return a pointer. A special property of arrays is that they return a pointer to the first element when used.

``` cpp
int ia[] = {0,1,2,3,4,5,6,7,8,9}; // ia is an array of ten ints
auto ia2(ia); // ia2 is an int* that points to the first element in ia
ia2 = 42; // error: ia2 is a pointer, and we can't assign an int to a pointer

auto ia2(&ia[0]); // now it's clear that ia2 has type int*
```

The increment operator can be used to move from one element to then next. When iterating over the elements in an array, attention must be payed not to dereference or increment the off-the-end pointer. The type returned by subtraction of two pointers is ptrdiff_t. Comparison operations on pointers must be performed on related objects.

IMPORTANT: A pointer returns the address of the object that it points to. That is why adding pointers is not only illegal but also meaningless.

An array can be used to initialize a vector, but arrays cannot be initialized using a vector.

``` cpp
int int_arr[] = {0, 1, 2, 3, 4, 5};
// ivec has six elements; each is a copy of the corresponding element in int_arr
vector<int> ivec(begin(int_arr), end(int_arr));
```

Takeaway: It is usually best to just use vectors in modern c++ instead of arrays and pointers.

## 3.6. Multidimensional Arrays