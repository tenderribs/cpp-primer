#include <iostream>
#include <array>
using std::cout;
using std::cin;
using std::endl;

int main() {
    int integer = 42;
    int* p = &integer;
    cout << "integer: " << integer << endl;
    cout << "p: " << p << endl;
    cout << "*p: " << *p << endl;
    return 0;
}