#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::getline;

int main() {
    string string1;
    string string2;

    getline(cin, string1);
    getline(cin, string2);

    if (string1 == string2) {
        cout << "Strings are the same" << endl;
    } else {
        cout << string1 << " and " << string2 << " arent the same" << endl;
    }
    return 0;
}