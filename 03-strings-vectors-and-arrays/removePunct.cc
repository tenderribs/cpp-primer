#include <iostream>
#include <string>
#include <cctype>

using std::cin;
using std::cout;
using std::endl;
using std::string;

int main() {
    string sentence = "";
    getline(cin, sentence);

    for (auto c : sentence) {
        if (ispunct(c)) {
            c = ' ';
        }
    }

    cout << sentence;
}