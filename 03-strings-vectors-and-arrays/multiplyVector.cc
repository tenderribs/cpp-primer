#include <vector>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

using std::vector;

int main() {
    vector<int> numbers = {2, 4, 45, 67};

    for (auto it = numbers.begin(); it != numbers.end(); ++it) {
        *it *= 2;
        cout << *it << endl;
    }

    return 0;
}