#include <vector>
#include <string>

using std::vector;
using std::string;

int main() {
    vector<vector<int>> ivec;
    vector<string> svec(10, "null");
    vector<int> v3(10, 42); // 10 el with val 42
    vector<int> v4{10}; // 1 el with val 10¨

    vector<string> v6{10}; // 10 default init strings
    vector<string> v7{10, "hi"}; // 10 el with val "hi"

    vector<int> ivec;
}
