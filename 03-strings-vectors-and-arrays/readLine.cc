#include <string>
#include <iostream>

using std::string;
using std::cout;
using std::cin;
using std::endl;
using std::getline;

int main() {
    string word;

    while (getline(cin, word)) {
        if (!word.empty()) {
            cout << word << " " << word.size() << endl;
        }
    }

    return 0;
}