#include <iostream>
#include <stdexcept>

using std::cin;
using std::cout;
using std::endl;

int main() {
    int num1, num2;

    cout << "Please enter two ints for division" << endl;
    cin >> num1 >> num2;

    try {
        if (num2 == 0)
            throw std::runtime_error("divide by zero");
        cout << num1 / num2 << endl;
    }
    catch (std::runtime_error err)
    {
        cout << err.what();
    }
}