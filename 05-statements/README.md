# 5 Statements
## 5.1 Simple Statements

### The Null Statement
The null statement is simply a semicolon and can be used when the language requires a statement, but the program's logic doesnt. A use case is the read an input stream while condition not true and evaluate ; in statement body. Here the logic already is in the head.

### Compound Statements (Blocks)

Compound statements are used when the language requires one statement but the logic of the program requires more than one. For example a loop encloses statements in its body using curly braces. The sequence of statements is a block.

## 5.2 Statement Scope

Variables defined in the control structure of a statement have block scope and are visible only within that statement.

## 5.3 Conditional Statements

The if statement I skipped for my documentation

### The switch statement
The switch statement is convenient for selecting among a large number of fixed alternatives.

The syntax is
```cpp
switch(var) {
    case 'var1': // case label
        /* do something */
        break;
}
```
The case label has to be of integral type. Case 3.14 will not work. The program flows over all case statements even after a label is matched. The break statement interrupts the flow and exits the switch.

The default label is executed when no case label matches the value of the switch statement.

```cpp
switch (ch) {
    case 'a': case 'e': case 'i': case 'o': case 'u':
        ++vowelCnt;
        break;
    default:
        ++otherCnt;
        break;
    }
}
```

### `While` statement

Variables defined in a while condition or body are created and destroyed on each iteration. The while statement is helpful when it is uncertain how many times the loop will execute.


### `For` statement

The top part of a for loop is called a for header. The init-statement, condition or expression can be replaced by using a null statement.

With a ranged for it is easy to process all elements in a container type. If write access is required, the loop variable must be a reference type. It is basically the C++ version of foreach.

### The `do` while Statement

The do while statement is similar to the while statement, but the body is executed before the condition is tested. The loop is therefore at least executed once.
```cpp
    do
    statement
    while (condition);
```

## 5.5 Jump Statements

The purpose of jump statements are to interrupt the flow of the program.

- A break will terminate the nearest enclosing while, do while, for or switch.
Continue terminates the current iteration and jumps to the next one of the enclosing loop. While loops will jump to the condition. For will jump to next expression in header. Range for will init
- next var from sequence.

## 5.5 `try` Blocks and Exception Handling

Exception handling allows cooperation betwen the detecting and handling parts of a program. `throw` expressions indicate an expception has been encountered. `try` blocks try to handle the exception and has one or many `catch` clauses.

The syntax of a try catch exception handler is the following:

```cpp
try {
    /* program statement with normal program logic that is to be executed */
} catch {
    /* handler statements to deal with exception */
}
```
Declarations stil have block scope, so declarations in the try program statements wont be visible to the catch clause.
```cpp
while (cin >> item1 >> item2) {
    try {
        // execute code that will add the two Sales_items
        // if the addition fails, the code throws a runtime_error exception
    } catch (runtime_error err) {
        // remind the user that the ISBNs must match and prompt for another pair
        cout << err.what()
        << "\nTry Again? Enter y or n" << endl;
        char c;
        cin >> c;
        if (!cin || c == 'n')
            break; // break out of the while loop
    }
}
```
Functions are exited in search of a handler. Once all functions are exited, the program crashes intentionally.

<b>The stdexcept header defines several general-purpose exception classes</b>

```cpp
#include <stdexcept>
runtime_error //problem that can only be detected at runtime
range_error //run time error result generated outside the range of values that are meaningful

overflow_error //run time error: computation that overflowed
underflow_error //run time error: computation that underflowed
logic_error //error in the logic of the program.
```

The exception types only define a single operation named `what`, that returns a const char* to a c-style character string.